package com.manager.students;


import com.manager.students.models.Student;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.manager.students.repositories.StudentRepository;

@Component
public class DbSeeder implements CommandLineRunner{

    @Autowired
    private StudentRepository _studentRepository;


    @Override
    public void run(String... strings) throws Exception {
        _studentRepository.deleteAll();

        
        Student student1 = new Student("sami.benabderazak@etudiant-fst.utm.tn", "Sami", "Ben Abderazak");
        Student student2 = new Student("dexter.morgan@gmail.com", "Dexter", "Morgan");
        Student student3 = new Student("toto@gmail.com", "toto", "yolo");

        _studentRepository.save(student1);
        _studentRepository.save(student2);
        _studentRepository.save(student3);




        System.out.println("___________________________________");
        System.out.println("Test MongoDB repository");
        System.out.println("Find student(s) by first name");
        _studentRepository.findByFirstName("Sami").forEach(System.out::println);
        System.out.println("___________________________________");
    }
}
