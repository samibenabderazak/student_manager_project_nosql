package com.manager.students.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manager.students.models.Student;
import com.manager.students.repositories.StudentRepository;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/api/student/")
public class StudentService {
    @Autowired
    private StudentRepository _studentRepository;

    @GetMapping(path = "students-list")
    public List<Student> getAllCategoriesFromMongoDB(){
        return _studentRepository.findAll();
    }
    
    @GetMapping(path = "/{student_id}")
    public Student getStudent(@PathVariable("student_id") String student_id){
        return _studentRepository.findById(student_id).orElse(new Student());
    }

    @PostMapping(path = "save-student")
    public void insert(@RequestBody Student student) {
    	_studentRepository.save(student);
    }
    
    @DeleteMapping("delete-student/{student_id}")
    public void deleteStudent(@PathVariable("student_id") String student_id) {
        _studentRepository.deleteById(student_id);
    }  

    @PostMapping("update-student/{student_id}")
    public void updateStudent(@RequestBody Student student, @PathVariable("student_id") String student_id) {
        student.setId(student_id);
        _studentRepository.save(student);
    }  
}
