This is my work for the NoSQL project.  
It is a Student Manager with CRUD functionalities and an interactive search bar using MongoDB as a database.  
Video presentation of the project:  
https://youtu.be/VQvRq75oXJ8  

Technology stack:  
Angular is used for the Frontend server  
Spring Boot is used for the Backend server  
MongoDB is used as a database  

There are two main pages:  
-One to see the students "View Students"  
-One to add students "Add Students"  

On the page where you can see students there are only 6 entries "students" at a time and you can go through them with a "next" and "previous" buttons to see the rest.  

You can search, update and delete the students on the "View students" page.  
On the "Add students" page, you fill a from and submit then you can do it again by clicking on "add more students".  

DBseeder.java is used to initialise the database by emptying it ".deleteAll()" and adding three students to test it. So if you want the data to persist after relauching the backend server just edit this file.  

To configure the connection with the MongoDB database go to the properties file under "Student Manager Backend/src/main/resources "
and change these settings:  
spring.data.mongodb.uri=mongodb://localhost:27017/local  
spring.data.mongodb.port=27017  


View students page:  
![view_students](/uploads/8af7476b5935a879db7dc82016ff8b03/view_students.png)

Add students page:  
![add_student](/uploads/103adac7c54134952f5d67c769f58bf7/add_student.png)


Sami Ben Abderazak IF5 ISIAD
