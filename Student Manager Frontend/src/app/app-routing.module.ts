import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentListComponent } from './students-list/student-list.component';
import { AddStudentComponent } from './add-student/add-student.component';


const routes: Routes = [
  { path: '', redirectTo: 'view-students', pathMatch: 'full' },
  { path: 'view-students', component: StudentListComponent },
  { path: 'add-student', component: AddStudentComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
