import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

  private baseUrl = 'http://localhost:8080/api/student/';

  constructor(private http:HttpClient) { }

  getStudentList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+'students-list');
  }

  createStudent(student: object): Observable<object> {
    return this.http.post(`${this.baseUrl}`+'save-student', student);
  }

  deleteStudent(id: string): Observable<any> {
    console.log(id);
    return this.http.delete(`${this.baseUrl}delete-student/${id}`, { responseType: 'text' });
  }


  getStudent(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}${id}`);
  }

  updateStudent(id: string, value: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}update-student/${id}`, value);
  }

}
