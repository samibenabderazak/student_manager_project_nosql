import { Component, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';
import {FormControl,FormGroup} from '@angular/forms';
import { Student } from '../models/student';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  constructor(private studentservice:StudentService) { }

  student : Student=new Student();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  studentsaveform=new FormGroup({
    accountId:new FormControl(),
    firstName:new FormControl(),
    lastName:new FormControl(),
  });

  saveStudent(saveStudent){
    this.student=new Student();
    this.student.accountId=this.StudentAccountId.value;
    this.student.firstName=this.StudentFirstName.value;
    this.student.lastName=this.StudentLastName.value;
    this.submitted = true;
    this.save();
  }



  save() {
    this.studentservice.createStudent(this.student)
      .subscribe(data => console.log(data), error => console.log(error));
    this.student = new Student();
  }

  get StudentAccountId(){
    return this.studentsaveform.get('accountId');
  }

  get StudentFirstName(){
    return this.studentsaveform.get('firstName');
  }

  get StudentLastName(){
    return this.studentsaveform.get('lastName');
  }

  addStudentForm(){
    this.submitted=false;
    this.studentsaveform.reset();
  }
}
