import { Component, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';
import { Student } from '../models/student';
import { Observable,Subject } from "rxjs";

import {FormControl,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

 constructor(private studentservice:StudentService) { }

  studentArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();


  students: Observable<Student[]>;
  student : Student=new Student();
  updStudent: Student=new Student();
  deleteMessage=false;
  studentslist:any;
  isupdated = false;


  ngOnInit() {
    this.isupdated=false;
    this.dtOptions = {
      pageLength: 6,
      stateSave:true,
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };
    this.studentservice.getStudentList().subscribe(data =>{
    this.students =data;
    this.dtTrigger.next();
    })
  }

  deleteStudent(id: string) {
    this.studentservice.deleteStudent(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.studentservice.getStudentList().subscribe(data =>{
            this.students =data
            })
        },
        error => console.log(error));
  }

  showStudent(student: Student){
    this.updStudent = student;
    console.log(this.isupdated);
  }


  studentupdateform=new FormGroup({
    id:new FormControl(),
    accountId:new FormControl(),
    firstName:new FormControl(),
    lastName:new FormControl(),
  });

  updateStudent(updsel){
   this.student=new Student();
   this.student.id=this.StudentId.value;
   this.student.accountId=this.StudentAccountId.value;
   this.student.firstName=this.StudentFirstName.value;
   this.student.lastName=this.StudentLastName.value;
   console.log(this.StudentId.value);


   this.studentservice.updateStudent(this.student.id,this.student).subscribe(
    data => {
      this.isupdated=true;
      this.studentservice.getStudentList().subscribe(data =>{
        this.students =data
        })
    },
    error => console.log(error));
  }
  get StudentId(){
    return this.studentupdateform.get('id');
  }
  get StudentAccountId(){
    return this.studentupdateform.get('accountId');
  }

  get StudentFirstName(){
    return this.studentupdateform.get('firstName');
  }

  get StudentLastName(){
    return this.studentupdateform.get('lastName');
  }

  changeisUpdate(){
    this.isupdated=false;
  }
}
